include Rpareia

DEFAULT_FILE = "spec/fixtures/empty_file.xml"

def xml_dedup(task: 'deduplication', ids: [0], files: [DEFAULT_FILE], types: ['delimited'], field_separators: ["\t"], fields: [[{name: 'id', type: 'int'}]], parts: [{:'field-name' => 'id'}], deterministic: true, conjunction: 1, output_deterministic: ['foo'])
  xml(task: task, ids: ids, files: files, types: types, field_separators: field_separators, fields: fields, parts: parts, deterministic: deterministic, conjunction: conjunction, output_deterministic: output_deterministic)
end

def xml_linkage(task: 'linkage', ids: [0,1], files: [DEFAULT_FILE, DEFAULT_FILE], types: ['delimited', 'delimited'], field_separators: ["\t", "\t"], fields: [[{name: 'id', type: 'int'}], [{name: 'id', type: 'int'}]], parts: [{:'field-name' => 'id'}], deterministic: true, conjunction: 1, output_deterministic: ['foo'])

  xml(task: task, ids: ids, files: files, types: types, field_separators: field_separators, fields: fields, parts: parts, deterministic: deterministic, conjunction: conjunction, output_deterministic: output_deterministic)
end

def xml(task:, ids: [], files: nil, types: nil, field_separators: nil, fields: nil, parts: nil, deterministic: true, conjunction: 1, output_deterministic: ['foo'])
  Nokogiri::XML::Builder.new do |xml|
    xml.project(task: task) {
      xml.send('data-sources') {
        ids.each_with_index do |id,i|
          xml.send('data-source', id: id, file: files[i], type: types[i], :'field-separator' => field_separators[i]) {
            xml.fields {
              next unless fields[i]
              fields[i].each do |field|
                xml.field(field)
              end
            }
          }
        end
      }
      if deterministic
        xml.send('deterministic-linkage') {
          conjunction.times do
            xml.conjunction {
              parts.each do |part|
                xml.part(part)
              end
            }
          end
        }
      end
      if output_deterministic
        if output_deterministic.empty?
          xml.output
        else
          output_deterministic.each do |out|
            xml.output(deterministic: out)
          end
        end
      end
    }
  end.to_xml
end

RSpec.describe Parser do
  context "initialization" do
    it "expects a xml string as argument" do
      expect {Parser.new}.to raise_error(ArgumentError)
    end
    it "complains about invalid XML" do
      bad_xml = '<?xml version="1.0 encoding="UTF-8"?>'
      expect {Parser.new(bad_xml)}.to raise_error(Parser::SyntaxError)
    end
  end

  context "tasks" do
    it "accepts only valid tasks" do
      xml = xml_dedup
      expect {Parser.new(xml)}.to_not raise_error

      xml = xml_linkage
      expect {Parser.new(xml)}.to_not raise_error
    end

    it "rejects invalid tasks" do
      xml = xml_dedup(task: 'foo')
      expect {Parser.new(xml)}.to raise_error(Parser::InvalidTaskError, "Invalid task: 'foo'")
    end
  end

  context "data source" do
    it "accepts only one data sources when deduplicating" do
      xml = xml_linkage(task: 'deduplication')
      expect {Parser.new(xml)}.to raise_error(Parser::InvalidNumberOfSources, "Deduplication: expected one data-sources, 2 given")
    end

    it "accepts only two data sources when linking" do
      xml = xml_dedup(task: 'linkage')
      expect {Parser.new(xml)}.to raise_error(Parser::InvalidNumberOfSources, "Linkage: expected two data-source, 1 given")
    end

    it "rejects data sources without id" do
      xml = xml_dedup(ids: [nil])
      expect {Parser.new(xml)}.to raise_error(Parser::MissingDataSourceId)
    end

    it "rejects two data sources with same id" do
      xml = xml_linkage(ids: [1,1])
      expect {Parser.new(xml)}.to raise_error(Parser::DuplicatedDataSourceId, "Duplicated data source id '1'")
    end

    it "expects a file name" do
      xml = xml_dedup(files: [''])
      expect {Parser.new(xml)}.to raise_error(Parser::MissingDataSourceFile, "Missing file attribute from data source '0'")
    end

    it "rejects invalid file names" do
      xml = xml_dedup(files: ['invalid'])
      expect {Parser.new(xml)}.to raise_error(Parser::InvalidDataSourceFile, "File 'invalid' from data source '0' does not exist")
    end

    it "accepts only 'delimiter' type" do
      xml = xml_dedup(types: ['delimited'])
      expect {Parser.new(xml)}.to_not raise_error

      xml = xml_dedup(types: ['foo'])
      expect {Parser.new(xml)}.to raise_error(Parser::InvalidDataSourceType, "Data source type 'foo' not supported")
    end

    it "expects a field-separator" do
      xml = xml_dedup(field_separators: ["\t"])
      expect {Parser.new(xml)}.to_not raise_error

      xml = xml_dedup(field_separators: [""])
      expect {Parser.new(xml)}.to raise_error(Parser::MissingFieldSeparator, "Missing field separator from data source '0'")
    end

    it "expects a 'fields' entry" do
      xml = xml_dedup(fields: [[{name: 'id', type: 'int'}]], parts: [{:'field-name' => 'id'}])
      expect {Parser.new(xml)}.to_not raise_error

      xml = xml_dedup(fields: [])
      expect {Parser.new(xml)}.to raise_error(Parser::FieldsElementNotFound)
    end

    it "expect a name attribute on field element" do
      xml = xml_dedup(fields: [[{name: '', type: 'int'}]])
      expect {Parser.new(xml)}.to raise_error(Parser::MissingFieldName)
    end

    it "rejects duplicated names on the same data source" do
      xml = xml_dedup(fields: [[{name: 'foo', type: 'int'},{name: 'foo', type: 'int'}]])
      expect {Parser.new(xml)}.to raise_error(Parser::DuplicatedFieldName, "Duplicated field name 'foo' on data source '0'")
    end

    it "expect a valid field type" do
      ['int', 'string'].each do |type|
        xml = xml_dedup(fields: [[{name: 'foo', type: type}]], parts: [{:'field-name' => 'foo'}])
        expect {Parser.new(xml)}.to_not raise_error
      end

      xml = xml_dedup(fields: [[{name: 'foo', type: 'bar'}]])
      expect {Parser.new(xml)}.to raise_error(Parser::InvalidFieldType, "Invalid type 'bar' from field 'foo', data source '0'")
    end
  end

  context "deterministic" do
    it "expects a deterministic-linkage element" do
      xml = xml_dedup(deterministic: false)
      expect {Parser.new(xml)}.to raise_error(Parser::DeterministicLinkageElementNotFound, "Missing deterministic-linkage element")
    end

    it "expects a single conjunction element" do
      xml = xml_dedup(conjunction: 0)
      expect {Parser.new(xml)}.to raise_error(Parser::ConjunctionElementNotFound, "Missing conjunction element")

      xml = xml_dedup(conjunction: 1)
      expect {Parser.new(xml)}.to_not raise_error

      conjunction = 2
      xml = xml_dedup(conjunction: conjunction)
      expect {Parser.new(xml)}.to raise_error(Parser::MultipleConjunctionElements, "Only one conjunction element is allowed, #{conjunction} found")
    end

    it "expects at leas one part element" do
      xml = xml_dedup(fields: [[{name: 'foo', type: 'int'}]], parts: [{:'field-name' => 'foo'}])
      expect {Parser.new(xml)}.to_not raise_error

      xml = xml_dedup(parts: [])
      expect {Parser.new(xml)}.to raise_error(Parser::MissingPart, "At leas one part element is required")
    end

    it "expects a field-name on part elements" do
      xml = xml_dedup(fields: [[{name: 'foo', type: 'string'}]], parts: [{:'field-name' => 'foo'}])
      expect {Parser.new(xml)}.to_not raise_error

      xml = xml_dedup(parts: [{not_field_name: 'foo'}])
      expect {Parser.new(xml)}.to raise_error(Parser::MissingFieldName, "Missing attribute field-name on part element")
    end

    it "accepts only field-names present on all data sources" do
      xml = xml_linkage(fields: [[{name: 'foo', type: 'string'}],[{name: 'foo', type: 'string'}]], parts: [{:'field-name' => 'foo'}])
      expect {Parser.new(xml)}.to_not raise_error

      xml = xml_linkage(fields: [[{name: 'foo', type: 'string'}],[{name: 'bar', type: 'string'}]], parts: [{:'field-name' => 'foo'}])
      expect {Parser.new(xml)}.to raise_error(Parser::MissingPartFieldNameOnDataSource, "Field name 'foo' not found on data source '1'")
    end
  end

  context "output" do
    it "expects a single 'output' element" do
      xml = xml_dedup(output_deterministic: ["foo"])
      expect {Parser.new(xml)}.to_not raise_error

      xml = xml_dedup(output_deterministic: ["foo","bar"])
      expect {Parser.new(xml)}.to raise_error(Parser::MultipleOutputElement, "Only one output element is allowed, 2 found")

      xml = xml_dedup(output_deterministic: false)
      expect {Parser.new(xml)}.to raise_error(Parser::MissingOutputElement, "Missing output element")
    end

    it "expects a deterministic attribute on output element" do
      xml = xml_dedup(output_deterministic: "")
      expect {Parser.new(xml)}.to raise_error(Parser::MissingDeterministicAttribute, "Missing attribute 'deterministic' on output element")
    end
  end
end
