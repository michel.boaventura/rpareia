class Heartbeat
  def self.check(addr:, ctx:, type:, debug: false)
    if(type == 'client')
      hb = ctx.socket(:REQ)
      hb.verbose = debug
      hb.connect(addr)
      hb.send("")
      hb.close
    else
      hb = ctx.socket(:REP)
      hb.verbose = debug
      hb.bind(addr)
      hb.recv
      hb.close
    end
  end
end
