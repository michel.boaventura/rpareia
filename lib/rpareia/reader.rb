require 'bundler/setup'
require 'rbczmq'
require_relative 'heartbeat'
require 'pry'

class Reader
  BLOCKER_PORT_START = 2000
  CTX = ZMQ::Context.new

  def initialize(num_readers:, num_blocks:, rank:, file:, debug: false, addr: "tcp://localhost")
    @num_readers = num_readers
    @num_blocks = num_blocks
    @rank = rank
    @file = file
    @debug = debug
    @addr = addr
  end

  def start
    start_connections
    read_database
  end

  def start_connections

    @connections = []

    0.upto(@num_blocks - 1) do |i|
      port = BLOCKER_PORT_START + i
      Heartbeat.check(addr: "#{@addr}:#{2 * port}", ctx: CTX, type: 'client', debug: @debug)

      @connections << create_socket(port)
    end
  end

  def create_socket(port)
    socket = CTX.socket(:DEALER)
    socket.verbose = @debug
    socket.connect("#{@addr}:#{port}")
    return socket
  end

  def read_database
    i = -1
    File.open(@file,'r').each_line do |line|
      i += 1
      next if i % @num_readers != @rank

      pair = create_pair(line.strip)
      send_pair(pair)
    end
    close_connections
    puts "EOF"
  end

  def close_connections
    @connections.each do |c|
      c.send("EOF")
      c.close
    end
    CTX.destroy
  end

  def send_pair(pair)
    block_id = which_block(pair.last)

    @connections[block_id].send(pair.join(','))
  end

  def create_hash(input)
    input.join
  end

  def which_block(key)
    key.bytes.first % @num_blocks
  end

  def create_pair(input)
    line = input.split(',')
    [line.first, create_hash(line[1..-1])]
  end
end

Reader.new(
  debug: false,
  num_readers: 2,
  num_blocks: 1,
  rank: ARGV[0].to_i,
  file: (ARGV[1] || "input/10m.csv"),
  addr: (ARGV[2] || "tcp://localhost")
).start
