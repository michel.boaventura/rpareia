require_relative 'heartbeat'
require 'rbczmq'
require 'pry'

class Blocker

  BLOCKER_PORT_START = 2000
  CTX = ZMQ::Context.new

  def initialize(rank:, debug: false)
    @rank = rank
    @debug = debug
    @port = BLOCKER_PORT_START + @rank
  end

  def start
    start_hb
    connect

    total = 0

    loop do
      msg = @socket.recv

      next if msg.nil? || msg[0] == "\x00"

      if msg != "EOF"
        total += 1
      else
        puts "TOTAL=#{total}"
      end
    end

  end

  def start_hb
    Thread.new do
      loop do
        Heartbeat.check(addr: "tcp://*:#{2 * @port}", ctx: CTX, type: 'server', debug: @debug)
      end
    end
  end

  def connect
    @socket = CTX.socket(:ROUTER)
    @socket.verbose = @debug
    @socket.bind("tcp://*:#{@port}")

  end
end

Blocker.new(rank: ARGV[0].to_i).start
