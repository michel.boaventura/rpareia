[![CodeClimate](https://codeclimate.com/github/michelboaventura/rpareia/badges/gpa.svg)](https://codeclimate.com/github/michelboaventura/rpareia)
[![Travis](https://travis-ci.org/michelboaventura/rpareia.svg)](https://travis-ci.org/michelboaventura/rpareia.svg)

# Rpareia

Pareia's[1] implementation in Ruby with zeromq:

[1] https://github.com/michelboaventura/pareia

## Installation

    $ gem install rpareia

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it ( https://github.com/michelboaventura/rpareia/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
