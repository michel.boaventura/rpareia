require File.join([File.dirname(__FILE__),'lib','rpareia','version.rb'])

Gem::Specification.new do |s|
  s.name          = "rpareia"
  s.description   = "A data deduplication software, based on Pareia"
  s.version       = Rpareia::VERSION
  s.authors       = ["Michel Boaventura"]
  s.email         = ["michel.boaventura@gmail.com"]
  s.homepage      = "http://github.com/michelboaventura/rpareia"
  s.platform      = Gem::Platform::RUBY
  s.summary       = %q{Pareia implementation, with ruby and ZeroMQ}
  s.files         = `git ls-files -z`.split("\x0")
  s.license       = "MIT"
  s.require_paths = ["lib"]
  s.bindir        = 'bin'
  s.executables   = ['rpareia']
  s.test_files    = s.files.grep(%r{^(spec)/})

  s.add_development_dependency "bundler", "~> 1.7"
  s.add_development_dependency "rake", "~> 10.0"
  s.add_development_dependency "pry", "~> 0.10"
  s.add_development_dependency "rspec", "~> 3.2"
  s.add_development_dependency "guard-rspec", "~> 4.5"

  s.add_dependency "nokogiri", "~> 1.6"
  s.add_dependency "rbczmq", "~> 1.7"
  s.add_dependency "gli", "~> 2.12"
end
